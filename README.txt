This plugin for confluence adds a Publish as Blog button to all pages.

This allows you to copy your page to a blog in the same space.